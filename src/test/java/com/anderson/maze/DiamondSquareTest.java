package com.anderson.maze;

import com.anderson.maze.*;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestResult;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class DiamondSquareTest extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public DiamondSquareTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(DiamondSquareTest.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void test5by5() {
        DiamondSquare ds = new DiamondSquare();
        Array2D a = ds.gen(5, 5, 0.0f, 100.0f, 0.5f);

        for(int x = a.minX(); x <= a.maxX(); ++x) {
            for (int y = a.minY(); y < a.maxY(); ++y) {
                System.out.print(a.get(y,x));
                System.out.print("\t");
            }

            System.out.println();
        }
        
        assertTrue(true);
    }

    public static void main(String[] args) {
        TestResult result;
        DiamondSquareTest.suite().run(result);
    }
}
