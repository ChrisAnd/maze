package com.anderson.maze;

public class DiamondSquare implements Generate {

	public Array2D gen(int height, int width, float min, float max, float dampening) {
        Array2D map = new Array2D(height, width);

        // Seed the four corners of the map.
        map.set(0, 0, Util.next(min, max));
        map.set(0, width - 1, Util.next(min, max));
        map.set(height - 1, width - 1, Util.next(min, max));
        map.set(height - 1, 0, Util.next(min, max));

        gen_internal(map, 0,0, height-1, width-1, max-min, dampening);
        
        return map;
	}

    private void gen_internal(Array2D map, int y1, int x1, int y2, int x2, float range, float dampening) {
        // See https://en.wikipedia.org/wiki/Diamond-square_algorithm
        if (y2-y1 <= 1 || x2-x1 <= 1) {
            return;
        }

        int midX = (x2-x1) / 2;
        int midY = (y2-y1) / 2;

        // Diamond step - Average four corners and add random and put in the midpoint
        range *= dampening;
        float mid = (map.get(y1, x1) + map.get(y1, x2) + map.get(y2, x1) + map.get(y2, x2)) / 4;
        map.set(midY, midX, Util.pivot(mid, range));

        // Square step 
        int radiusX = midX - x1;
        int radiusY = midY - y1;
        range *= dampening;
        calc_square(map, midY, x1, radiusY, radiusX, range); // left side
        calc_square(map, y1, midX, radiusY, radiusX, range); // top side
        calc_square(map, midY, x2, radiusY, radiusX, range); // right side
        calc_square(map, y2, midX, radiusY, radiusX, range); // bottom side

        // Recurse
        gen_internal(map, y1, x1, midY, midX, range, dampening); // Upper left
        gen_internal(map, y1, midX, midY, x2, range, dampening); // Upper right
        gen_internal(map, midY, midX, y2, x2, range, dampening); // Lower right
        gen_internal(map, midY, x1, y2, midX, range, dampening); // Lower left
    }

    private void calc_square(Array2D map, int y, int x, int radiusY, int radiusX, float range) {
        float mid = 0.0f;
        mid += map.get_wrap(y, x - radiusX); // left
        mid += map.get_wrap(y - radiusY, x); // top
        mid += map.get_wrap(y, x + radiusX); // right
        mid += map.get_wrap(y + radiusY, x); // bottom

        map.set(y, x, Util.pivot(mid / 4, range));
    }

}