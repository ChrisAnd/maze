package com.anderson.maze;

public class Array2D {
    private int height;
    private int width;
    private float a[][];
    
    public Array2D(int height, int width) {
        a = new float[height][width];
        this.height = height;
        this.width = width;
    }

    public float get(int y, int x) {
        return a[y][x];
    }

    public float get_wrap(int y, int x) {
        return a[wrap(y, width)][wrap(x, height)];
    }

    public void set(int y, int x, float val) {
        a[y][x] = val;
    }

    public void add(int y, int x, float val) {
        a[y][x] += val;
    }

    private static int wrap(int val, int max) {
        if (val < 0)
            return val * -1;
        else if (val > max - 1)
            return val - max;
        
        return val;
    }

    public int minX() {
        return 0;
    }

    public int minY() {
        return 0;
    }

    public int maxX() {
        return this.width -1;
    }

    public int maxY() {
        return this.height -1;
    }

    public int height() {
        return this.height;
    } 

    public int width() {
        return this.width;
    }
}   