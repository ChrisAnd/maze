package com.anderson.maze;

import java.awt.Color;
import java.awt.Graphics2D;

public class Geography {
    Array2D map;

    public Geography() {
        DiamondSquare ds = new DiamondSquare();
        this.map = ds.gen(5, 5, 0.0f, 255.0f, 0.5f);
    }

    public void draw(Graphics2D g) {
        double width = 2000;
        double height = 2000;
        double stepX = width / this.map.width();
        double stepY = height / this.map.height();

        for(int x = this.map.minX(); x <= this.map.maxX(); ++x) {
            for (int y = this.map.minY(); y < this.map.maxY(); ++y) {
                float val = this.map.get(y, x);
                Color c = new Color(val, val, val);
                g.setColor(c);
                g.drawRect((int)(x * stepX), (int)(y * stepY), (int)(x * stepX + stepX), (int)(y * stepY + stepY));
            }
        }
    }
}