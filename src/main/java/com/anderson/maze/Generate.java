package com.anderson.maze;

public interface Generate {
    public Array2D gen(int height, int width, float min, float max, float dampening);    
}